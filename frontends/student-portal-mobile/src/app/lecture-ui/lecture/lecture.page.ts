import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NavController } from "@ionic/angular";

@Component({
  selector: "app-lecture",
  templateUrl: "./lecture.page.html",
  styleUrls: ["./lecture.page.scss"]
})
export class LecturePage implements OnInit {
  constructor(private navCtrl: NavController) {}

  ngOnInit() {
    // this.route.navigateByUrl("lecture/current-lecture");
    this.navCtrl.navigateRoot("lecture/current-lecture");
  }

  navigateBack() {
    this.navCtrl.pop();
  }
}
