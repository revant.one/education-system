import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviousLecturePage } from './previous-lecture.page';

describe('PreviousLecturePage', () => {
  let component: PreviousLecturePage;
  let fixture: ComponentFixture<PreviousLecturePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviousLecturePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviousLecturePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
